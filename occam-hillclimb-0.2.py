import random, json, copy, math, sys, os
from bisect import bisect
from subprocess import call
from parser import Parser, ParserDRSIM2Bandwidth
import os

DEFAULT_CONVERGANCE_THRESHOLD = 5
LOGFILE_NAME = "Hillclimb.log"
numRuns = 0
prsr = ParserDRSIM2Bandwidth()

#define convergance threshold
if len(sys.argv)<4:
	CONVERGANCE_THRESHOLD = DEFAULT_CONVERGANCE_THRESHOLD
else:
	CONVERGANCE_THRESHOLD = int(sys.argv[3])

def writeLog(lStr):
	logFile = open(LOGFILE_NAME, 'a')
	logFile.write(lStr)
	logFile.close()

def writeConfig(c):
	string = json.dumps(c)
	f= open("configurations/0/input.json", "w")
	f.write(string)
	f.close()
	#commit change
	print(c)
	call(["git","add","./configurations/0/input.json"])
	call(["git","commit", "-m", "'HillclimbOptimizer'"])
	call(["occam", "configure", "0"])
def calculateBandwidth(oJson):
	totalBandwidth = 0
	chanList = oJson["channels"]
	for chan in chanList:
		totalBandwidth+= chan["bandwidth"]
	writeLog("Candidite Bw: " + str(totalBandwidth))
	return totalBandwidth

def RunAndCalculateOutput(inputJson):
	#write out output
	writeConfig(inputJson)
	call(["rm", "-rf", "./runs/0/outputs/"])
	call(["occam","run"])
	dirlist = os.walk("./runs/0/outputs")
	jsonDirName = ""
	#find directory name
	for x in dirlist:
		if x[0][:28] == "./runs/0/outputs/application":
			jsonDirName = x[0][:70] 


	try:
		writeLog(jsonDirName + "/output.json")
		f = open(jsonDirName+"/output.json")
		jStr = f.read()
		f.close()
		outputJson = json.loads(jStr)
	except FileNotFoundError:
		writeLog("error- no output found\n")
		return 0
	#return random.random()
	return calculateBandwidth(outputJson)


#FUNCTION: weighted_select
#Description: Randomly selects a configuration in a fashion weighted by the amount of increase.
#PARAMS:
#opts: a list of (Dictionary, Float) tuples representing the pairing of a configuration and its improved bandwidth
#returns: a tuple of the chosen configuration and its improvement.
def weighted_select(opts):
	values,weights = zip(*opts)
	wtotal = 0
	cum_weights=[]
	for w in weights:
		wtotal +=w
		cum_weights.append(wtotal)
	r = random.random() * total
	n = bisect(cum_weights, r)
	return (values[n],weights[n])
def mutate(inputJsonObj):
	new = []
	generatedInputs = []
	#permute each value
	for item in inputJsonObj:
		#if can be mutable
		if item in acceptable:
			newInput = copy.deepcopy(inputJsonObj)
			#change value
			newInput[item] = random.choice(acceptable[item])
			#print(newInput[item])
			new.append(newInput)
	return new
#Dummy function to calculate a bandwidth number for rapid testing of hill climbing.

#TODO
#move acceptable values object into a configuration object
def readInAcceptableInputs(filename):
	f = open(filename)
	jsonStr = f.read()
	f.close()


	returnval = json.loads(jsonStr)

	

	return returnval


acceptable = readInAcceptableInputs(sys.argv[1])

#acceptable = {'jedec_data_bus_bits': jdbbAccept,'num_chans': ["1","2","3"],'cmd_queue_depth': ["1","2","4","8","16","32"], 'memory_size':["2048","4096","1"]}

#Take in an input configuration, and for each variable in it, generate a novel configuration with only that variable changed



	
#read in initial config
f= open(sys.argv[2])
configtext=f.read()
f.close()

config = json.loads(configtext)

pastConfigs = []


#clear log file
f= open(LOGFILE_NAME, "w")
f.write("Log\n")
f.close()

#MAIN LOOP

numTimesNoImprovement = 0
lastinput = config
lastbandwidth = RunAndCalculateOutput(lastinput)
#go through epochs
for i in range(0, 1000):
	if (numTimesNoImprovement>=CONVERGANCE_THRESHOLD):
		print("CONVERGED ON MAXIMUM")
		print(numRuns)
		exit(0)
	potentials=mutate(lastinput)
	results = []
	total = 0
	options = []
	#print "Options"
	#print potentials
	#for each potential configuration
	for trial in potentials:
		if trial not in pastConfigs:
			pastConfigs.append(trial)
			print
			bandwidth = prsr.RunAndCalculateOutput(trial)
			numRuns+=1
			#if improvement
			if bandwidth>lastbandwidth:
				#add to list for hill climbing
				results.append((trial,bandwidth))
				total+=bandwidth
			#if any improvements are found
			if (len(results)>0):
				numTimesNoImprovement = 0
				newchoice = weighted_select(results)
				print ('SELECTED')
				print (newchoice)
				lastinput = newchoice[0]
				lastbandwidth = newchoice[1]
				writeLog("New optimum: " + str(lastbandwidth)+ "\n")
			else:
				print ("NO IMPROVEMENT")
				numTimesNoImprovement+=1			
	
	


