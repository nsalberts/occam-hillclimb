import json, copy, math, sys, os
def calculateBandwidth(oJson):
	totalBandwidth = 0
	chanList = oJson["channels"]
	for chan in chanList:
		totalBandwidth+= chan["bandwidth"]
	#writeLog("Candidite Bw: " + str(totalBandwidth))
	return totalBandwidth

def RunAndCalculateBandwidth():
	#write out output
	#writeConfig(inputJson)
	#call(["rm", "-rf", "./runs/0/outputs/"])
	#call(["occam","run"])

	dirlist = os.walk("./runs/0/outputs")
	jsonDirName = ""
	#find directory name
	for x in dirlist:
		if x[0][:28] == "./runs/0/outputs/application":
			jsonDirName = x[0][:70] 


	try:
		f = open(jsonDirName+"/output.json")
		jStr = f.read()
		f.close()
		outputJson = json.loads(jStr)
	except FileNotFoundError:
		print("error- no output found")
		return 0
	#return random.random()
	return calculateBandwidth(outputJson)

print (str(RunAndCalculateBandwidth()))
